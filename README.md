# My Shell Scripts

These are my personal shell scripts for day to day use. Some of them are already outdated (meaning I don't use them for a while) and some are exclusive to my setup.

Most of them are just simple shortcuts to optimize my time, so many of these scripts depend on pre installed (non-basic) software for linux. Some of those are:
- the surf web browser from [suckless](https://suckless.org)
- mupdf to open pdf files
- flac (mostly to convert from wave files)

## escanear

The "escanear" script is part of an ongoin project to make easier for me to scan parts of books that I'm consulting in my PhD thesis. I got the idea from the [NAPS2](https://www.naps2.com) project, which is a windows gui software to control scanners. In NAPS2 you can set the ammount of pages to scan a wait time between scanners, making book scanning a lot easier. 

To be fully functional to me, escanear needs to have an automatic OCR and pdf conversion script.

Currently the script depends on scanimage, which is part of [SANE](http://www.sane-project.org), but the OCR and pdf conversion should need [tesseract](https://github.com/tesseract-ocr) and [imagemagick's convert](https://github.com/tesseract-ocr) which normally are all software available as packages in your distro's repositories.