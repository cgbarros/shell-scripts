#!/bin/bash

swayidleID=$(pidof swayidle)

echo "$swayidleID"

if [ -z "$swayidleID" ]
then	
	swayidle -w \
		timeout 300 'swaylock -f -c 000000' \
		timeout 600 'swaymsg "output * dpms off"' \
		resume 'swaymsg "output * dpms on"' \
		before-sleep 'swaylock -f -c 000000'
else	
	kill $swayidleID
fi
